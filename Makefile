DOCUMENT := rapport

.PHONY: run
run:
	$(MAKE) all
	$(MAKE) all

.PHONY: all
all:
	$(MAKE) compile_pdf
	$(MAKE) compile_glossary
	$(MAKE) compile_bibliography
	$(MAKE) compile_pdf

.PHONY: compile_pdf
compile_pdf:
	pdflatex $(DOCUMENT).tex

.PHONY: compile_glossary
compile_glossary:
	makeindex -s $(DOCUMENT).ist -o $(DOCUMENT).gls $(DOCUMENT).glo

.PHONY: compile_bibliography
compile_bibliography:
	bibtex $(DOCUMENT).aux

.PHONY: clean
clean:
	rm -f *.log *.aux *.out *.pdf *.gz *.toc *.gls *.glo *.ist *.ilg *.bbl *.blg *.glg *.run.xml *.bcf *blx*
